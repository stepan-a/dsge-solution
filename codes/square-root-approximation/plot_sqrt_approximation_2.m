addpath ../../matlab2tikz/src

MaxOrder = 5;

abscissa =  0:.01:2;
epsilon  = abscissa-1;

y = NaN(MaxOrder,length(epsilon));

true_y = sqrt(abscissa);

for order=1:MaxOrder
    syms x;
    sqrt_approximation = taylor(sqrt(1+x),'Order',order+1);
    x = epsilon;
    y(order,:) = abs(eval(sqrt_approximation)-true_y);
end

syms x;
sqrt_approximation = taylor(sqrt(1+x),'Order',20);
xx = .0:.05:2;
x = xx-1;
y20 = abs(eval(sqrt_approximation)-sqrt(xx));

base_color = .5*[1,1,1];

hold on
for order=1:MaxOrder
    plot(abscissa,y(order,:),'Color',base_color-order/10); % Approximationn of the true function
end
plot(xx,y20,'+r');
hold off



legend('First order','Second order','Third order','Fourth order','Fifth order','Twentieth order','Location','NorthEast')
box on

axis tight

set(gca,'XTick',[0:.2:2])
set(gca,'XTickLabel',{'-1.0'; '-0.8'; '-0.6'; '-0.4'; '-0.2'; '0.0'; '0.2'; '0.4'; '0.6'; '0.8'; '1.0'});

set(gca,'YTick',[.1:.1:.5])
set(gca,'YTickLabel',{'0.1'; '0.2'; '0.3'; '0.4'; '0.5'});

matlab2tikz('../tex/sqrt_approximation_2.tikz', 'height', '\figureheight', 'width', '\figurewidth');
